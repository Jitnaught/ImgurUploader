Option Explicit

dim wss: set wss = WScript.CreateObject("WScript.Shell")
dim fso: set fso = CreateObject("Scripting.FileSystemObject")

dim appDataDir
appDataDir = wss.expandEnvironmentStrings("%APPDATA%")

dim linkPath
linkPath = fso.BuildPath(appDataDir, "\Microsoft\Windows\SendTo\Imgur Uploader.lnk")

if (fso.FileExists(linkPath)) Then
	fso.DeleteFile(linkPath)
	MsgBox("Send To entry deleted")
else
	MsgBox("Send To entry does not exist")
end if