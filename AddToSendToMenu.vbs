Option Explicit

dim wss: set wss = WScript.CreateObject("WScript.Shell")
dim fso: set fso = WScript.CreateObject("Scripting.FileSystemObject")

dim currentDir
currentDir = fso.GetAbsolutePathName(".")

dim appDataDir
appDataDir = wss.expandEnvironmentStrings("%APPDATA%")

dim linkPath
linkPath = fso.BuildPath(appDataDir, "\Microsoft\Windows\SendTo\Imgur Uploader.lnk")

dim targetPath
targetPath = fso.BuildPath(currentDir, "ImgurUploader.exe")

dim link: set link = wss.CreateShortcut(linkPath)
link.TargetPath = targetPath
link.Save

MsgBox("Send To entry created")