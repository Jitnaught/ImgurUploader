﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;

namespace ImgurUploader
{
    public partial class ProgressForm : Form
    {
        public ProgressForm()
        {
            InitializeComponent();
            fileListProgressBar.Maximum = uploadProgressBar.Maximum = 1000;
        }

        /// <summary>
        /// 0-1.0 representation of our progress through this current upload
        /// </summary>
        public double UploadProgress { get; set; }
        public string UploadMessage { get; set; }

        /// <summary>
        /// 0-1.0 representation of our progress through the list of files to upload
        /// </summary>
        public double FilesProgress { get; set; }
        public string FilesMessage { get; set; }

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            fileListProgressBar.Value = (int)Math.Floor(FilesProgress * 1000);
            uploadProgressBar.Value = (int)Math.Floor(UploadProgress * 1000);

            fileListPercent.Text = String.Format("{0:0}%", FilesProgress * 100);
            uploadPercent.Text = String.Format("{0:0}%", UploadProgress * 100);

            fileListName.Text = FilesMessage;
            uploadName.Text = UploadMessage;
        }

        delegate void AddLinkDel(ImgurUploadInfo u);
        public void AddLink(ImgurUploadInfo u)
        {
            if (this.InvokeRequired)
            {
                AddLinkDel d = new AddLinkDel(AddLink);
                this.Invoke(d, new object[] { u });
                return;
            }

            Uploaded.Add(u);
            AddLabelAndLink(u.file, "", false);
            AddLabelAndLink("Image Link:", u.link, true);
            AddLabelAndLink("Delete Image Link:", "http://imgur.com/delete/" + u.deletehash, true);
        }

        List<ImgurUploadInfo> Uploaded = new List<ImgurUploadInfo>();

        private void AddLabelAndLink(string label, string link, bool pad)
        {
            Label t = new Label
            {
                Text = label,
                Height = 14,
                AutoSize = (!pad),
                Padding = (pad) ? new Padding(25, 0, 0, 0) : new Padding(0, 5, 0, 0),
                Width = 100
            };
            linksPanel.Controls.Add(t);

            LinkLabel l = new LinkLabel
            {
                Text = link,
                AutoSize = true,
                Padding = new Padding(0),
            };

            l.Click += new EventHandler((object s, EventArgs e) => { Process.Start(link); });
            linksPanel.Controls.Add(l);
            linksPanel.SetFlowBreak(l, true);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void clipboardCopyLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (Uploaded == null || Uploaded.Count == 0) return;

            string clipTxt = "";
            //public string id { get; set; }
            //public string deletehash { get; set; }
            //public string link { get; set; }
            //public string file { get; set; }
            Uploaded.ForEach(u => clipTxt += string.Format("File: {0}\nID: {1}\nDelete hash: {2}\nImage Link: {3}\nDelete Image Link: {4}\n\n", u.file, u.id, u.deletehash, u.link, "http://imgur.com/delete/" + u.deletehash));

            Clipboard.SetText(clipTxt.Trim());
        }

        private void ProgressForm_Load(object sender, EventArgs e)
        {
            this.BringToFront();
        }
    }
}
