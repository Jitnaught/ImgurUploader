﻿namespace ImgurUploader
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProgressForm));
            this.fileListProgressBar = new System.Windows.Forms.ProgressBar();
            this.uploadProgressBar = new System.Windows.Forms.ProgressBar();
            this.uploadName = new System.Windows.Forms.Label();
            this.uploadPercent = new System.Windows.Forms.Label();
            this.fileListName = new System.Windows.Forms.Label();
            this.fileListPercent = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.updateTimer = new System.Windows.Forms.Timer(this.components);
            this.linkListLabel = new System.Windows.Forms.Label();
            this.linksPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.clipboardCopyLabel = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // fileListProgressBar
            // 
            this.fileListProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileListProgressBar.Location = new System.Drawing.Point(12, 54);
            this.fileListProgressBar.Name = "fileListProgressBar";
            this.fileListProgressBar.Size = new System.Drawing.Size(396, 23);
            this.fileListProgressBar.TabIndex = 0;
            // 
            // uploadProgressBar
            // 
            this.uploadProgressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.uploadProgressBar.Location = new System.Drawing.Point(12, 12);
            this.uploadProgressBar.Name = "uploadProgressBar";
            this.uploadProgressBar.Size = new System.Drawing.Size(396, 23);
            this.uploadProgressBar.TabIndex = 0;
            // 
            // uploadName
            // 
            this.uploadName.AutoSize = true;
            this.uploadName.Location = new System.Drawing.Point(9, 38);
            this.uploadName.Name = "uploadName";
            this.uploadName.Size = new System.Drawing.Size(159, 13);
            this.uploadName.TabIndex = 1;
            this.uploadName.Text = "Checking for validity / uploading";
            // 
            // uploadPercent
            // 
            this.uploadPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uploadPercent.Location = new System.Drawing.Point(345, 38);
            this.uploadPercent.Name = "uploadPercent";
            this.uploadPercent.Size = new System.Drawing.Size(63, 13);
            this.uploadPercent.TabIndex = 1;
            this.uploadPercent.Text = "1%";
            this.uploadPercent.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // fileListName
            // 
            this.fileListName.AutoSize = true;
            this.fileListName.Location = new System.Drawing.Point(12, 80);
            this.fileListName.Name = "fileListName";
            this.fileListName.Size = new System.Drawing.Size(88, 13);
            this.fileListName.TabIndex = 1;
            this.fileListName.Text = "Doing Something";
            // 
            // fileListPercent
            // 
            this.fileListPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fileListPercent.Location = new System.Drawing.Point(333, 80);
            this.fileListPercent.Name = "fileListPercent";
            this.fileListPercent.Size = new System.Drawing.Size(75, 13);
            this.fileListPercent.TabIndex = 1;
            this.fileListPercent.Text = "27%";
            this.fileListPercent.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.Location = new System.Drawing.Point(333, 109);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // updateTimer
            // 
            this.updateTimer.Enabled = true;
            this.updateTimer.Interval = 500;
            this.updateTimer.Tick += new System.EventHandler(this.updateTimer_Tick);
            // 
            // linkListLabel
            // 
            this.linkListLabel.AutoSize = true;
            this.linkListLabel.Location = new System.Drawing.Point(9, 129);
            this.linkListLabel.Name = "linkListLabel";
            this.linkListLabel.Size = new System.Drawing.Size(41, 13);
            this.linkListLabel.TabIndex = 4;
            this.linkListLabel.Text = "So Far:";
            // 
            // linksPanel
            // 
            this.linksPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linksPanel.AutoScroll = true;
            this.linksPanel.BackColor = System.Drawing.Color.White;
            this.linksPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linksPanel.Location = new System.Drawing.Point(12, 145);
            this.linksPanel.Name = "linksPanel";
            this.linksPanel.Size = new System.Drawing.Size(396, 108);
            this.linksPanel.TabIndex = 5;
            // 
            // clipboardCopyLabel
            // 
            this.clipboardCopyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.clipboardCopyLabel.Location = new System.Drawing.Point(183, 262);
            this.clipboardCopyLabel.Name = "clipboardCopyLabel";
            this.clipboardCopyLabel.Size = new System.Drawing.Size(225, 17);
            this.clipboardCopyLabel.TabIndex = 6;
            this.clipboardCopyLabel.TabStop = true;
            this.clipboardCopyLabel.Text = "copy upload information to clipboard";
            this.clipboardCopyLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.clipboardCopyLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.clipboardCopyLabel_LinkClicked);
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 288);
            this.Controls.Add(this.clipboardCopyLabel);
            this.Controls.Add(this.linksPanel);
            this.Controls.Add(this.linkListLabel);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.fileListPercent);
            this.Controls.Add(this.uploadPercent);
            this.Controls.Add(this.fileListName);
            this.Controls.Add(this.uploadName);
            this.Controls.Add(this.uploadProgressBar);
            this.Controls.Add(this.fileListProgressBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProgressForm";
            this.Text = "Imgur Upload Progress";
            this.Load += new System.EventHandler(this.ProgressForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar fileListProgressBar;
        private System.Windows.Forms.ProgressBar uploadProgressBar;
        private System.Windows.Forms.Label uploadName;
        private System.Windows.Forms.Label uploadPercent;
        private System.Windows.Forms.Label fileListName;
        private System.Windows.Forms.Label fileListPercent;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Timer updateTimer;
        private System.Windows.Forms.Label linkListLabel;
        private System.Windows.Forms.FlowLayoutPanel linksPanel;
        private System.Windows.Forms.LinkLabel clipboardCopyLabel;
    }
}