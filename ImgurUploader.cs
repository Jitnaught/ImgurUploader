﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Net;
using System.Xml;
using System.Collections.Specialized;
using System.Windows.Forms;

namespace ImgurUploader
{
    public class ImgurUploader
    {
        public delegate void UploadStatusHandler(object source, ImgurUploaderStatus s);
        public event UploadStatusHandler UpdateStatus;
        
        public delegate void FileCompleteHandler(object source, ImgurUploadInfo s);
        public event FileCompleteHandler FileComplete;

        public List<string> Files { get; set; }

        public void UploadFiles()
        {
            int currentFileNumber = 0;

            foreach (string file in Files)
            {
                currentFileNumber++;
                string TotalMessage = string.Format("Dealing with file {0}/{1}", currentFileNumber, Files.Count);
                double TotalProgress = (currentFileNumber - 1) / (Files.Count * 1.0f);
                ImgurUploaderStatus status =
                    new ImgurUploaderStatus
                    {
                        TotalMessage = TotalMessage,
                        TotalProgress = TotalProgress,
                        FileBeingProcessed = Path.GetFileName(file)
                    };
                try
                {
                    status.FileProcessProgress = 0;

                    status.FileProcessMessage = "Verifying existence";
                    UpdateStatus(this, status);

                    if (!File.Exists(file)) throw new Exception("File could not be found");

                    status.FileProcessMessage = "Verifying file integrity";
                    UpdateStatus(this, status);

                    try
                    {
                        Image.FromFile(file);
                    }
                    catch (ArgumentException)
                    {
                        throw new Exception("File did not appear to be an image");
                    }

                    var client = new WebClient();
                    
                    client.Headers.Add("Authorization", "Client-ID " + "d24b200b0d8df11");
                    var values = new NameValueCollection
                        {
                            { "image", Convert.ToBase64String(File.ReadAllBytes(file)) }
                        };

                    byte[] response = client.UploadValues("https://api.imgur.com/3/upload.xml", values);

                    client.Dispose();

                    status.FileProcessMessage = "File sent.";
                    status.FileProcessProgress = 1;
                    UpdateStatus(this, status);

                    XmlDocument responseXml = new XmlDocument();
                    responseXml.LoadXml(Encoding.Default.GetString(response));

                    if (responseXml["data"].Attributes["success"].Value == "1")
                        FileComplete(this, new ImgurUploadInfo
                        {
                            link = responseXml["data"]["link"].InnerText,
                            deletehash = responseXml["data"]["deletehash"].InnerText,
                            id = responseXml["data"]["id"].InnerText,
                            file = file
                        });
                    else
                        throw new Exception("Wasn't able to upload to Imgur.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }

            UpdateStatus(this, new ImgurUploaderStatus
            {
                FileBeingProcessed = "All Finished",
                FileProcessMessage = "",
                FileProcessProgress = 1,
                TotalMessage = "All Files Done",
                TotalProgress = 1
            });
        }
    }

    [Serializable]
    public class ImgurUploadInfo
    {
        public string id { get; set; }
        public string deletehash { get; set; }
        public string link { get; set; }
        public string file { get; set; }
    }

    public class ImgurUploaderStatus
    {
        public string FileBeingProcessed { get; set; }
        public string FileProcessMessage { get; set; }
        public double FileProcessProgress { get; set; }

        public string TotalMessage { get; set; }
        public double TotalProgress { get; set; }
    }
}
