Fork of the Imgur Uploader program made by iamhrh. 

His program hasn't been updated since 2010, and wasn't able to upload images, so I fixed that, and also made a few other changes.

Installation:
* Install .NET Framework 4.5.2.
* Download [the latest archive](https://gitlab.com/Jitnaught/ImgurUploader/tags) and extract it to a folder.
* (Optional) Run AddToSendToMenu.vbs to add the program to the Send To context menu.

![Picture uploaded](https://i.imgur.com/GVy7oG2.jpg)

![Picture about to be uploaded](https://i.imgur.com/QuxOIhh.jpg)

(I used this program to upload these images to Imgur. :) )
